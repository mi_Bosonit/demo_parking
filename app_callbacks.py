from dash import callback
from dash.dependencies import Input, Output

import pages

lista_de_paginas = {
    f"/aparcamientos": pages.aparcamientos.aparcamientos.layout,
    f"/admin": pages.admin.admin.layout,
}


# Update page content
@callback(
    Output("app_content", "children"),
    Input("url", "pathname"),
)
def display_page(pathname):
    if pathname in lista_de_paginas:
        return lista_de_paginas[pathname]
    else:
        return "paginas disponibles: '/aparcamientos' y '/admin' "
