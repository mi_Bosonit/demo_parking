from dash import Dash, dcc
from flask import Flask

from app_callbacks import *
from assets.app_css import *
from assets.my_dash.my_html.my_div import my_div

app = Dash(__name__)
app.config.suppress_callback_exceptions = True

app.layout = my_div(
    style_app,
    "",
    [
        dcc.Location(id="url"),
        dcc.Store(id="store"),
        my_div(style_content_app, "app_content"),
    ],
)


if __name__ == "__main__":
    app.run_server(debug=True)  # , use_reloader=True)
