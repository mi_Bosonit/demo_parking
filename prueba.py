from pandarallel import pandarallel
import pandas as pd
from numpy.random import randint

df = pd.DataFrame(
    {
        "a": randint(0,100, size=10000000),
        "b": randint(0,100, size=10000000),
        "c": randint(0,100, size=10000000),
        "d": randint(0,100, size=10000000),
        "e": randint(0,100, size=10000000),
        "f": randint(0,100, size=10000000),
        "g": randint(0,100, size=10000000),
        "h": randint(0,100, size=10000000),
        "i": randint(0,100, size=10000000),
        
    }
)
print(df)
pandarallel.initialize(progress_bar=True)
df.parallel_apply(lambda x: x**4)
print("--------------")


from tqdm.notebook import tqdm_notebook
tqdm_notebook.pandas()

print(df)
df.progress_apply(lambda x: x**4)
print("--------------")