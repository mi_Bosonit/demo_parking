from datetime import datetime, timedelta
from assets.app_css import color_celdas_tabla
from dash import dash_table

percentage = dash_table.FormatTemplate.percentage(2)

columnas_monitorizacion_parking = [
    dict(id="description", name="Localizacion"),
    dict(id="status", name="Estado"),
    dict(id="total_spot_number", name="Plazas"),
    dict(id="available_spot_number", name="Libres"),
    dict(
        id="porcentaje_ocupacion",
        name="% de ocupacion",
        type="numeric",
        format=percentage,
    ),
    dict(id="actualizado", name="Última Actualización"),
]


# Nivel de llenado
def data_bars(df, column):
    n_bins = 99
    bounds = [i * (1.0 / n_bins) for i in range(n_bins + 1)]
    ranges = [(i * (100 / n_bins)) for i in bounds]
    styles = []
    for i in range(1, len(bounds)):
        min_bound = ranges[i - 1]
        max_bound = ranges[i]
        max_bound_percentage = bounds[i] * 100
        styles.append(
            {
                "if": {
                    "filter_query": (
                        "{{{column}}} >= {min_bound}"
                        + (
                            " && {{{column}}} < {max_bound}"
                            if (i < len(bounds) - 1)
                            else ""
                        )
                    ).format(column=column, min_bound=min_bound, max_bound=max_bound),
                    "column_id": column,
                },
                "background": (
                    """
                    linear-gradient(90deg,
                    rgb(86, 166, 75) 0%,
                    rgb(48, 94, 42) 40%,
                    rgb(173, 130, 95) 60%,
                    rgb(224, 47, 68) {max_bound_percentage}%,
                    #f8f8f8 {max_bound_percentage}%
                    )
                """.format(
                        max_bound_percentage=max_bound_percentage
                    )
                ),
                "paddingBottom": 2,
                "paddingTop": 2,
                "textAlign": "right",
                "color": "black",
            }
        )
    return styles


# Nivel de llenado
llenado = [
    {
        "if": {
            "column_id": ["porcentaje_ocupacion"],
            "column_id": ["porcentaje_ocupacion"],
        },
        "border-right": "1px solid rgba(36, 41, 46, 0.12)",
        "textAlign": "right",
    }
]

for x in range(81, 101):
    llenado.append(
        {
            "if": {
                "filter_query": "{{porcentaje}}={}".format(x),
                "column_id": ["porcentaje_ocupacion"],
            },
            "font-weight": "600",
            "background": """linear-gradient(90deg,
                             rgb(86, 166, 75) 0%,
                             rgb(48, 94, 42) 40%,
                             rgb(173, 130, 95) 60%,
                             rgb(224, 47, 68) {max_bound_percentage}%,
                             #f8f8f8 {white}%)
                       """.format(
                max_bound_percentage=x, white=x + 1
            ),
        }
    )
for x in range(61, 81):
    llenado.append(
        {
            "if": {
                "filter_query": "{{porcentaje}}={}".format(x),
                "column_id": ["porcentaje_ocupacion"],
            },
            "background": """linear-gradient(90deg,
                          rgb(86, 166, 75) 0%,
                          rgb(48, 94, 42) 40%,
                          rgb(173, 130, 95) 60%,
                          rgb(224, 47, 68) {max_bound_percentage}%,
                          #f8f8f8 {white}%)
                       """.format(
                max_bound_percentage=x, white=x + 1
            ),
        }
    )
for x in range(61):
    llenado.append(
        {
            "if": {
                "filter_query": "{{porcentaje}}={}".format(x),
                "column_id": ["porcentaje_ocupacion"],
            },
            "background": """linear-gradient(90deg,
                              rgb(86, 166, 75) 0%,
                              rgb(48, 94, 42) {max_bound_percentage}%,
                              #f8f8f8 {white}%)
                           """.format(
                max_bound_percentage=x, white=x + 1
            ),
        }
    )
# Localizacion
localizacion = [
    {
        "if": {"column_id": ["description"], "column_id": ["description"]},
        "width": "13vmax",
        "min-width": "13vmax",
        "border-right": "1px solid rgba(36, 41, 46, 0.12)",
        "color": "white",
        "background": """linear-gradient(90deg, #04212c 0%, #062741 70%, #0f5779 90%)""",
    }
]

# Fecha
fecha = [
    {
        "if": {
            "filter_query": "{{Hora}} < {}".format(
                (datetime.now() - timedelta(hours=3)).time()
            ),
            "column_id": ["actualizado"],
        },
        "color": "#e8a809",
        "font-weight": "bold",
        "background": color_celdas_tabla,
        "font-size": "0.85vmax",
    },
    {
        "if": {
            "filter_query": "{{Hora}} >= {}".format(
                (datetime.now() - timedelta(hours=3)).time()
            ),
            "column_id": ["actualizado"],
        },
        "color": "#61b96d",
        "font-weight": "bold",
        "background": color_celdas_tabla,
        "font-size": "0.85vmax",
    },
    {
        "if": {
            "filter_query": "{{Fecha}} != {}".format(datetime.now().date()),
            "column_id": ["actualizado"],
        },
        "color": "#FD0703",
        "background": color_celdas_tabla,
        "font-size": "0.85vmax",
    },
]

# Estado
estado = [
    {
        "if": {
            "filter_query": "{{status}}={}".format("Abierto"),
            "column_id": ["status"],
        },
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "color": "#3ed21d",
        "background": "linear-gradient(90deg, #d3dcdc 0%, #f8f8f8 40%, #d3dcdc 80%)",
    },
    {
        "if": {
            "filter_query": "{{status}} != {}".format("Abierto"),
            "column_id": ["status"],
        },
        "font-family": "Roboto, Helvetica, Arial, sans-serif",
        "color": "#FD0703",
        "background": "linear-gradient(90deg, #d3dcdc 0%, #f8f8f8 40%, #d3dcdc 80%)",
    },
]

style_data_conditional_monitorizacion_parking = localizacion + fecha + estado + llenado


lista_de_localizaciones = [
    "Parking PARKIA - Alameda del Rio",
    "Aparcamiento público Centro Plaza",
    "Parking Las Albinas 1-2",
    "Aparcamiento público Plaza Mayor",
    "Aparcamiento el Manchón",
    "Aparcamiento público Juzgados de Chiclana",
    "Parking Público Urbisur",
    "Aparcamiento Zona Deportiva",
    "Aparcamiento Balneario Fuente Amarga",
    "Aparcamiento Viveros Infraplant",
    "Parque Retortillo",
    "Aparcamiento Centro de Salud Los Gallos",
    "Aparcamiento CASH FRESH",
    "Recinto ferial",
    "Aparcamiento Hotel Escuela Fuentemar",
]
