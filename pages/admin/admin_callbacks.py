from dash import callback, dcc
from dash.dependencies import Input, Output, State
from pandas import read_csv
from dash.exceptions import PreventUpdate
from assets.app_css import *
from assets.my_dash.my_html.my_div import my_div
from pages.admin.admin_utils import *
from pages.aparcamientos.aparcamientos_utils import crear_df, tabla_configuracion

id_page = "admin"


@callback(
    [
        Output(f"{id_page}_button_div_datos", "style"),
        Output(f"{id_page}_button_div_cuentas", "style"),
        Output(f"{id_page}_button_div_aparcamientos", "style"),
        Output(f"icon_datos", "icon"),
        Output(f"icon_cuentas", "icon"),
        Output(f"icon_aparcamientos", "icon"),
        Output(f"{id_page}_button_div_datos", "n_clicks"),
        Output(f"{id_page}_button_div_cuentas", "n_clicks"),
        Output(f"{id_page}_button_div_aparcamientos", "n_clicks"),
        Output(f"{id_page}_div_buttons_datos", "hidden"),
        Output(f"{id_page}_div_buttons_cuentas", "hidden"),
        Output(f"{id_page}_div_buttons_parkings", "hidden"),
        Output(f"{id_page}_panel_down_admin", "style"),
    ],
    [
        Input(f"{id_page}_button_div_datos", "n_clicks"),
        Input(f"{id_page}_button_div_cuentas", "n_clicks"),
        Input(f"{id_page}_button_div_aparcamientos", "n_clicks"),
    ],
    [
        State(f"{id_page}_button_div_datos", "style"),
        State(f"{id_page}_button_div_cuentas", "style"),
        State(f"{id_page}_button_div_aparcamientos", "style"),
        State(f"icon_datos", "icon"),
        State(f"icon_cuentas", "icon"),
        State(f"icon_aparcamientos", "icon"),
        State(f"{id_page}_panel_down_admin", "style"),
    ],
    prevent_initial_call=True,
)
def display_page(
    datos,
    cuentas,
    parkings,
    state_datos,
    state_cuentas,
    state_parkings,
    state_icon_datos,
    state_icon_cuentas,
    state_icon_parkings,
    state_panel_down,
):
    state_datos["border-bottom"] = "2px solid #919394"
    state_cuentas["border-bottom"] = "2px solid #919394"
    state_parkings["border-bottom"] = "2px solid #919394"
    state_panel_down["height"] = "90%"

    if parkings:
        state_parkings["border-bottom"] = "1px solid black"
        icon_datos = "carbon:caret-sort-down"
        icon_cuentas = "carbon:caret-sort-down"
        if state_icon_parkings == "carbon:caret-up":
            icon_parkings = "carbon:caret-sort-down"
            div_buttons_parkings_hidden = True
        else:
            icon_parkings = "carbon:caret-up"
            div_buttons_parkings_hidden = False
            state_panel_down["height"] = "73%"
        div_buttons_datos_hidden = True
        div_buttons_cuentas_hidden = True
    if datos:
        state_datos["border-bottom"] = "1px solid black"
        icon_cuentas = "carbon:caret-sort-down"
        icon_parkings = "carbon:caret-sort-down"
        if state_icon_datos == "carbon:caret-up":
            icon_datos = "carbon:caret-sort-down"
            div_buttons_datos_hidden = True
        else:
            icon_datos = "carbon:caret-up"
            div_buttons_datos_hidden = False
            state_panel_down["height"] = "73%"
        div_buttons_cuentas_hidden = True
        div_buttons_parkings_hidden = True

    if cuentas:
        state_cuentas["border-bottom"] = "1px solid black"
        icon_datos = "carbon:caret-sort-down"
        icon_parkings = "carbon:caret-sort-down"
        if state_icon_cuentas == "carbon:caret-up":
            icon_cuentas = "carbon:caret-sort-down"
            div_buttons_cuentas_hidden = True
        else:
            icon_cuentas = "carbon:caret-up"
            div_buttons_cuentas_hidden = False
            state_panel_down["height"] = "73%"
        div_buttons_datos_hidden = True
        div_buttons_parkings_hidden = True    
    return [
        state_datos,
        state_cuentas,
        state_parkings,
        icon_datos,
        icon_cuentas,
        icon_parkings,
        0,
        0,
        0,
        div_buttons_datos_hidden,
        div_buttons_cuentas_hidden,
        div_buttons_parkings_hidden,
        state_panel_down,
    ]


#### botones ####
@callback(
    [
        Output(f"{id_page}_button_parkings", "n_clicks"),
        Output(f"{id_page}_button_alertas", "n_clicks"),
        Output(f"{id_page}_button_parking_configuration", "n_clicks"),
        Output(f"{id_page}_button_configuracion_alertas", "n_clicks"),
        Output(f"{id_page}_button_mon_datos", "n_clicks"),
        Output(f"{id_page}_button_mon_cuentas", "n_clicks"),
        Output(f"{id_page}_button_parkings", "style"),
        Output(f"{id_page}_button_alertas", "style"),
        Output(f"{id_page}_button_parking_configuration", "style"),
        Output(f"{id_page}_button_configuracion_alertas", "style"),
        Output(f"{id_page}_button_mon_datos", "style"),
        Output(f"{id_page}_button_mon_cuentas", "style"),
        Output(f"{id_page}_panel_parkings", "hidden"),
        Output(f"{id_page}_panel_alertas", "hidden"),
        Output(f"{id_page}_panel_parking_configuration", "hidden"),
        Output(f"{id_page}_panel_configuracion_alertas", "hidden"),
        Output(f"{id_page}_panel_monitorizacion_datos", "hidden"),
        Output(f"{id_page}_panel_monitorizacion_cuentas", "hidden"),
    ],
    [
        Input(f"{id_page}_button_parkings", "n_clicks"),
        Input(f"{id_page}_button_alertas", "n_clicks"),
        Input(f"{id_page}_button_parking_configuration", "n_clicks"),
        Input(f"{id_page}_button_configuracion_alertas", "n_clicks"),
        Input(f"{id_page}_button_mon_datos", "n_clicks"),
        Input(f"{id_page}_button_mon_cuentas", "n_clicks"),
    ],
    [
        State(f"{id_page}_button_parkings", "style"),
        State(f"{id_page}_button_alertas", "style"),
        State(f"{id_page}_button_parking_configuration", "style"),
        State(f"{id_page}_button_configuracion_alertas", "style"),
        State(f"{id_page}_button_mon_datos", "style"),
        State(f"{id_page}_button_mon_cuentas", "style"),
    ],
)
def display_page(
    click_parkings,
    click_alertas,
    click_parking_configuration,
    click_configuracion_alertas,
    click_mon_datos,
    click_mon_cuentas,
    style_parkings,
    style_alertas,
    style_parking_configuration,
    style_configuracion_alertas,
    style_mon_datos,
    style_mon_cuentas,
):
    if click_parkings:
        style_parkings["border-bottom"] = "2px solid #232424"
        style_alertas["border-bottom"] = "2px solid #919394"
        style_parking_configuration["border-bottom"] = "2px solid #919394"
        style_configuracion_alertas["border-bottom"] = "2px solid #919394"
        style_mon_datos["border-bottom"] = "2px solid #919394"
        style_mon_cuentas["border-bottom"] = "2px solid #919394"
        panel_estadisticas_hidden = False
        panel_alertas_hidden = True
        panel_parking_configuration_hidden = True
        panel_configuracion_alertas = True
        panel_monitorizacion_datos = True
        panel_monitorizacion_cuentas = True

    elif click_alertas:
        style_parkings["border-bottom"] = "2px solid #919394"
        style_alertas["border-bottom"] = "2px solid #232424"
        style_parking_configuration["border-bottom"] = "2px solid #919394"
        style_configuracion_alertas["border-bottom"] = "2px solid #919394"
        style_mon_datos["border-bottom"] = "2px solid #919394"
        style_mon_cuentas["border-bottom"] = "2px solid #919394"
        panel_estadisticas_hidden = True
        panel_alertas_hidden = False
        panel_parking_configuration_hidden = True
        panel_configuracion_alertas = True
        panel_monitorizacion_datos = True
        panel_monitorizacion_cuentas = True

    elif click_parking_configuration:
        style_parkings["border-bottom"] = "2px solid #919394"
        style_alertas["border-bottom"] = "2px solid #919394"
        style_parking_configuration["border-bottom"] = "2px solid #232424"
        style_configuracion_alertas["border-bottom"] = "2px solid #919394"
        style_mon_datos["border-bottom"] = "2px solid #919394"
        style_mon_cuentas["border-bottom"] = "2px solid #919394"
        panel_estadisticas_hidden = True
        panel_alertas_hidden = True
        panel_parking_configuration_hidden = False
        panel_configuracion_alertas = True
        panel_monitorizacion_datos = True
        panel_monitorizacion_cuentas = True

    elif click_configuracion_alertas:
        style_parkings["border-bottom"] = "2px solid #919394"
        style_alertas["border-bottom"] = "2px solid #919394"
        style_parking_configuration["border-bottom"] = "2px solid #919394"
        style_mon_datos["border-bottom"] = "2px solid #919394"
        style_configuracion_alertas["border-bottom"] = "2px solid #232424"
        style_mon_cuentas["border-bottom"] = "2px solid #919394"
        panel_estadisticas_hidden = True
        panel_alertas_hidden = True
        panel_parking_configuration_hidden = True
        panel_configuracion_alertas = False
        panel_monitorizacion_datos = True
        panel_monitorizacion_cuentas = True

    elif click_mon_datos:
        style_parkings["border-bottom"] = "2px solid #919394"
        style_alertas["border-bottom"] = "2px solid #919394"
        style_parking_configuration["border-bottom"] = "2px solid #919394"
        style_configuracion_alertas["border-bottom"] = "2px solid #919394"
        style_mon_datos["border-bottom"] = "2px solid #232424"
        style_mon_cuentas["border-bottom"] = "2px solid #919394"
        panel_estadisticas_hidden = True
        panel_alertas_hidden = True
        panel_parking_configuration_hidden = True
        panel_configuracion_alertas = True
        panel_monitorizacion_datos = False
        panel_monitorizacion_cuentas = True

    elif click_mon_cuentas:
        style_parkings["border-bottom"] = "2px solid #919394"
        style_alertas["border-bottom"] = "2px solid #919394"
        style_parking_configuration["border-bottom"] = "2px solid #919394"
        style_configuracion_alertas["border-bottom"] = "2px solid #919394"
        style_mon_datos["border-bottom"] = "2px solid #919394"
        style_mon_cuentas["border-bottom"] = "2px solid #232424"
        panel_estadisticas_hidden = True
        panel_alertas_hidden = True
        panel_parking_configuration_hidden = True
        panel_configuracion_alertas = True
        panel_monitorizacion_datos = True
        panel_monitorizacion_cuentas = False
    else:
        raise PreventUpdate
    return [
        0,
        0,
        0,
        0,
        0,
        0,
        style_parkings,
        style_alertas,
        style_parking_configuration,
        style_configuracion_alertas,
        style_mon_datos,
        style_mon_cuentas,
        panel_estadisticas_hidden,
        panel_alertas_hidden,
        panel_parking_configuration_hidden,
        panel_configuracion_alertas,
        panel_monitorizacion_datos,
        panel_monitorizacion_cuentas
    ]


#### parkings
@callback(
    [
        Output(f"{id_page}_parkings_up", "children"),
        Output(f"{id_page}_parkings_down", "children"),
    ],
    Input(f"{id_page}_button_parkings", "n_clicks"),
)
def display_page(pathname):
    df = crear_df()[
        ["description", "available_spot_number", "total_spot_number", "porcentaje"]
    ]
    nuevos_nombres = ["Parking", "Plazas Libres", "Plazas Totales", "Ocupacion"]
    df.columns = nuevos_nombres

    divs_up = [
        my_div(
            style_figures_estats,
            "",
            figure,
        )
        for figure in [
            plazas_totales(df),
            parking_mas_ocupados(df),
            parking_menos_ocupados(df),
        ]
    ]

    obj_up = my_div(style_div_figures_stats, "", divs_up)
    df = crear_df()
    fig = mapa_admin(df, "markers", "available_spot_number", "description", 12)

    obj_down = dcc.Graph(
        figure=fig,
        style=style_graph_admin,
    )
    return [obj_up, obj_down]


#### Alertas ####
@callback(
    [
        Output(f"{id_page}_alertas_left", "children"),
        Output(f"{id_page}_alertas_right", "children"),
    ],
    Input(f"{id_page}_button_alertas", "n_clicks"),
)
def sel_ho(pathname):
    alertas = read_csv("alertas.csv")
    alerta = alertas[alertas["alert"] == "parkings"]["value"].iloc[0]
    df = crear_df()
    df_alertas = df.copy()
    df_alertas = df_alertas[df_alertas["available_spot_number"] <= alerta]
    parkings = df_alertas["description"]
    plazas_libres = df_alertas["available_spot_number"]
    fechas = df_alertas["actualizado"]

    obj = []
    for parking, plazas, fecha in zip(parkings, plazas_libres, fechas):
        obj.append(
            my_div(
                style_alerta,
                "",
                [
                    my_div(style_name_parking, "", f"{parking}"),
                    my_div(style_plazas_parking, "", f"plazas libres: {plazas}"),
                    my_div(
                        style_registro_parking, "", f"registro: {fecha.split('+')[0]}"
                    ),
                    my_div(style_registro_parking, "", "-------------------"),
                ],
            )
        )
    fig = mapa_alertas(df, alerta, 12)
    map = dcc.Graph(
        figure=fig,
        style=style_graph_admin,
    )
    return [my_div(style_panel_alertas, "", obj), map]


#### Configuración Aparcamientos
@callback(
    Output(f"{id_page}_panel_parking_configuration_tabla", "children"),
    Input(f"{id_page}_button_parking_configuration", "n_clicks"),
)
def sel_ho(pathname):
    df = read_csv("parking_information.csv")

    columnas_monitorizacion_parking = [
        dict(id="description", name="Aparcamiento"),
        dict(id="status", name="Estado", presentation="dropdown"),
        dict(id="barrier_access", name="Barrera", presentation="dropdown"),
        dict(id="free_access", name="Acceso Libre", presentation="dropdown"),
        dict(id="for_members", name="Para Miembros", presentation="dropdown"),
        dict(id="for_employees", name="Para Empleados", presentation="dropdown"),
        dict(id="for_disabled", name="Para Minusválidos", presentation="dropdown"),
        dict(id="total_spot_number", name="Plazas Totales"),
    ]
    open_closed = ["Abierto", "Cerrado"]
    yes_no = ["Si", "No"]
    dropdown_options = {
        "status": {"options": [{"label": i, "value": i} for i in open_closed]},
        "barrier_access": {"options": [{"label": i, "value": i} for i in yes_no]},
        "free_access": {"options": [{"label": i, "value": i} for i in yes_no]},
        "for_members": {"options": [{"label": i, "value": i} for i in yes_no]},
        "for_employees": {"options": [{"label": i, "value": i} for i in yes_no]},
        "for_disabled": {"options": [{"label": i, "value": i} for i in yes_no]},
    }

    return tabla_configuracion(
        "tabla_conf",
        df,
        columnas_monitorizacion_parking,
        style_table_configuracion_parkings,
        style_header,
        style_data,
        [],
        dropdown_options,
        page_size=9,
        row_selectable=False,
        editable=True,
    )


@callback(
    Output("store", "data"),
    Input("tabla_conf", "data"),
    State("store", "data"),
    prevent_initial_call=True,
)
def sel_ho(rows, data):
    if data is None:
        data = {}
    data["parkings"] = rows
    return data


@callback(
    Output("store", "data", allow_duplicate=True),
    Input(f"{id_page}_configuration_accept", "n_clicks"),
    State("store", "data"),
    prevent_initial_call=True,
)
def sel_ho(click, data):
    if click:
        try:
            data_parkings = data["parkings"]
        except:
            return data

        def validate_data(data):
            for row in data:
                for value in row.values():
                    if value is None or value == "":
                        return False
            return True

        columns = [
            "description",
            "status",
            "barrier_access",
            "free_access",
            "for_members",
            "for_employees",
            "for_disabled",
            "total_spot_number",
        ]
        df2 = DataFrame(columns=columns)
        if validate_data(data_parkings):
            df2 = DataFrame(data_parkings, columns=[c for c in columns])
            df = read_csv("parking_information.csv")[
                ["id", "lat", "long", "street_address", "description"]
            ]
            df2 = df2.merge(df, how="left", on="description")
            df2.to_csv("parking_information.csv", index=False)
    return data


@callback(
    Output(f"{id_page}_alertas_tabla", "children"),
    Input(f"{id_page}_button_configuracion_alertas", "n_clicks"),
)
def sel_ho(pathname):
    df = read_csv("alertas.csv")
    columnas_monitorizacion_parking = [
        dict(id="alert", name="Alerta"),
        dict(id="value", name="Umbral"),
    ]

    return tabla_configuracion(
        "tabla_alert",
        df,
        columnas_monitorizacion_parking,
        style_table_configuracion_parkings,
        style_header,
        style_data,
        [],
        dropdown_options={},
        page_size=15,
        row_selectable=False,
        editable=True,
    )


@callback(
    Output("store", "data", allow_duplicate=True),
    Input("tabla_alert", "data"),
    State("store", "data"),
    prevent_initial_call=True,
)
def sel_ho(rows, data):
    if data is None:
        data = {}
    data["alerts"] = rows
    return data


@callback(
    Output("store", "data", allow_duplicate=True),
    Input(f"{id_page}_alertas_accept", "n_clicks"),
    State("store", "data"),
    prevent_initial_call=True,
)
def sel_ho(click, data):
    if click:
        data_alert = data["alerts"]

        def validate_data(data):
            for row in data:
                for value in row.values():
                    if value is None or value == "":
                        return False
            return True

        columns = [
            "alert",
            "value",
        ]
        df2 = DataFrame(columns=columns)
        if validate_data(data_alert):
            df2 = DataFrame(data_alert, columns=[c for c in columns])
            df2.to_csv("alertas.csv", index=False)
    return data
