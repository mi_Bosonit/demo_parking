import plotly.express as px
from dash import dcc
from pandas import DataFrame

from assets.app_css import *
from pages.admin.admin_utils import *

id_page = "estadisticas"

style_graph = {
    "width": "100%",
    "height": "100%",
}

colorscale = [
    "rgb(255, 0, 0)",
    "rgb(255, 69, 0)",
    "rgb(255, 127, 0)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(60, 179, 60)",
    "rgb(34, 139, 34)",
    "rgb(0, 128, 0)",
]


def mapa_admin(df, mode, size_column, text_column, zoom=13, lat=0, lon=0):
    if lat == 0:
        lat = df["lat"].iloc[0]
    if lon == 0:
        lon = df["long"].iloc[0]
    df["text"] = "Plazas Libres: " + df[size_column].astype(str)

    hover_data = {
        "description": True,
        "available_spot_number": True,
        "total_spot_number": True,
        "lat": True,
        "long": True,
    }

    fig = (
        px.scatter_mapbox(
            df,
            lat="lat",
            lon="long",
            hover_name=text_column,
            hover_data=hover_data,
            size=size_column,
            color=size_column,
            color_continuous_scale=colorscale,
            zoom=zoom,
            center={"lon": lon, "lat": lat},
        )
        .update_layout(
            mapbox_style="open-street-map",
            margin={"r": 10, "t": 6, "l": 6, "b": 6},
            autosize=True,
            showlegend=False,
        )
        .update_traces(
            hovertemplate="""
                             <br>%{customdata[0]}
                             <br>Plazas libres: %{customdata[1]}
                             <br>Plazas Totales: %{customdata[2]}
                             <br>lat: %{customdata[1]}
                             <br>long: %{customdata[2]}""",
        )
    )
    return fig


def mapa_alertas(df, alarm, zoom=13, lat=0, lon=0):
    if lat == 0:
        lat = df["lat"].iloc[8]
    if lon == 0:
        lon = df["long"].iloc[8]

    template = ["red", "black"]
    alerts = ["Alerta" if spot <= alarm else "" for spot in df["available_spot_number"]]
    if "Alerta" not in alerts:
        template = ["black"]

    hover_data = {
        "description": True,
        "status": True,
        "available_spot_number": True,
        "total_spot_number": False,
    }

    fig = (
        px.scatter_mapbox(
            df,
            lat="lat",
            lon="long",
            color=alerts,
            color_discrete_sequence=template,
            hover_data=hover_data,
            hover_name=None,
            zoom=zoom,
            center={"lon": lon, "lat": lat},
        )
        .update_layout(
            mapbox_style="open-street-map",
            margin={"r": 2, "t": 0, "l": 2, "b": 0},
            autosize=True,
            showlegend=False,
        )
        .update_traces(
            marker={"size": 8},
            hovertemplate="""<br>%{customdata[0]}
                         <br>Estado: %{customdata[1]}
                         <br>Plazas Libres: %{customdata[2]}""",
        )
    )
    return fig


def plazas_totales(df):
    plazas_totales = df["Plazas Totales"].sum()
    plazas_libres = df["Plazas Libres"].sum()
    ocupacion = (plazas_totales - plazas_libres) / (plazas_totales) * 100

    df_tarta = DataFrame(
        {"Estado": ["Libre", "Ocupado"], "Ocupacion": [100 - ocupacion, ocupacion]}
    )

    fig = (
        px.pie(
            df_tarta,
            values="Ocupacion",
            names=["Libre", "Ocupado"],
            title="",
            hover_data=None,
            color="Estado",
            hole=0.05,
            color_discrete_sequence=["#6EC15F", "#CD774F"],
        )
        .update_layout(
            margin=dict(t=35, l=25, b=10, r=100),
            title="Nivel de ocupación de parkings",
            title_font=dict(size=15),
        )
        .update_traces(
            hovertemplate="<br>%{customdata[0]}",
        )
    )

    return dcc.Graph(figure=fig, style=style_graph)


def parking_mas_ocupados(df):
    top_5_parkings = df.nlargest(5, "Ocupacion").reset_index()

    fig = (
        px.bar(
            top_5_parkings,
            color_discrete_sequence=["#0CC1FA"],
            hover_data=["Parking", "Plazas Libres"],
            y="Ocupacion",
        )
        .update_layout(
            margin=dict(t=35, l=25, b=0, r=10),
            title="Parkings mas ocupados",
            bargap=0.1,
            title_font=dict(size=15),
        )
        .add_hline(
            y=100,
            line_dash="dash",
            line_color="rgb(255, 0, 0)",
            annotation_text="100% Ocupación",
        )
        .update_traces(width=0.8)
        .update_xaxes(visible=False)
    )

    return dcc.Graph(figure=fig, style=style_graph)


def parking_menos_ocupados(df):
    top_5_least_occupied = df.sort_values("Ocupacion").head(5).reset_index()
    fig = (
        px.bar(
            top_5_least_occupied,
            color_discrete_sequence=["#0CC1FA"],
            hover_data=["Parking", "Plazas Libres"],
            y="Ocupacion",
        )
        .update_layout(
            margin=dict(t=35, l=25, b=0, r=10),
            title="Parkings con más plazas libres",
            bargap=0.1,
            title_font=dict(size=15),
        )
        .add_hline(
            y=100,
            line_dash="dash",
            line_color="rgb(255, 0, 0)",
            annotation_text="100% Ocupación",
        )
        .update_traces(width=0.8)
        .update_xaxes(visible=False)
    )

    return dcc.Graph(figure=fig, style=style_graph)
