from assets.app_css import *
from assets.my_dash.my_dbc.my_button import my_button
from assets.my_dash.my_html.my_div import my_div
from pages.admin.admin_callbacks import *
from dash_iconify import DashIconify
from dash import html

id_page = "admin"

buttons_panel_up = [
    my_div(
        style,
        "",
        my_button(
            f"{id_page}_button_div_{id_button}",
            [
                html.P(
                    name,
                    style=style_name_buttom_panel_up,
                ),
                DashIconify(
                    id=f"icon_{id_button}",
                    icon="carbon:caret-sort-down",  
                    style=style_icon_buttom_panel_up,
                ),
            ],
            className="btn-panel",
            n_clicks=clicks,
        ),
    )
    for name, id_button, style, clicks in zip(
        ["Aparcamientos", "...", "..."],
        ["aparcamientos", "datos", "cuentas"],
        [
            style_div_button_panel_up,
            style_div_button_panel_up_right,
            style_div_button_panel_up_right,            
        ],
        [0, 0, 0],
    )
]


layout = [
    my_div(style_panel_up_admin, "", buttons_panel_up),
    # panels of buttons
    my_div(
        style_panel_buttons_parking,
        f"{id_page}_div_buttons_parkings",
        [
            my_div(
                style_div_button_parking,
                "",
                my_button(
                    f"{id_page}_button_{id_parking}",
                    name,
                    className="btn-panel",
                    n_clicks=0,
                ),
            )
            for name, id_parking in zip(
                [
                    "Monitorización",
                    "Configuración Aparcamientos",
                    "Alertas",
                    "Configuración Alertas",
                ],
                [
                    "parkings",
                    "parking_configuration",
                    "alertas",
                    "configuracion_alertas",
                ],
            )
        ],
        hidden=True,
    ),
    my_div(
        style_panel_buttons_datos,
        f"{id_page}_div_buttons_datos",
        [
            my_div(
                style_div_button_parking,
                "",
                my_button(
                    f"{id_page}_button_{id_parking}",
                    name,
                    className="btn-panel",
                    n_clicks=0,
                ),
            )
            for name, id_parking in zip(
                [
                    "...",
                ],
                [
                    "mon_datos",
                ],
            )
        ],
        hidden=True,
    ),
    my_div(
        style_panel_buttons_cuentas,
        f"{id_page}_div_buttons_cuentas",
        [
            my_div(
                style_div_button_parking,
                "",
                my_button(
                    f"{id_page}_button_{id_parking}",
                    name,
                    className="btn-panel",
                    n_clicks=0,
                ),
            )
            for name, id_parking in zip(
                [
                    "...",
                ],
                [
                    "mon_cuentas",
                ],
            )
        ],
        hidden=True,
    ),      
    # down panel
    my_div(
        style_panel_down_admin,
        f"{id_page}_panel_down_admin",
        [
            # datos
            my_div(
                style_div_alertas,
                f"{id_page}_panel_monitorizacion_datos",
                [my_div({}, "")],
                hidden=True,
            ),
            # cuentas
            my_div(
                style_div_alertas,
                f"{id_page}_panel_monitorizacion_cuentas",
                [my_div({}, "")],
                hidden=True,
            ),            
            # parkings
            my_div(
                style_panel_parkings,
                f"{id_page}_panel_parkings",
                [
                    my_div(style_panel_parkings_up, f"{id_page}_parkings_up"),
                    my_div(style_panel_parkings_down, f"{id_page}_parkings_down"),
                ],
                hidden=True,
            ),
            my_div(
                style_panel_parkings_configuration,
                f"{id_page}_panel_parking_configuration",
                [
                    my_div(
                        style_panel_parkings_configuration_table,
                        f"{id_page}_panel_parking_configuration_tabla",
                    ),
                    my_div(
                        style_div_button_config_accept,
                        "",
                        my_button(
                            f"{id_page}_configuration_accept",
                            "Guardar Cambios",
                            className="btn-accept",
                            n_clicks=0,
                        ),
                    ),
                ],
                hidden=True,
            ),
            my_div(
                style_div_alertas,
                f"{id_page}_panel_alertas",
                [
                    my_div(style_panel_alertas_left, f"{id_page}_alertas_left"),
                    my_div(style_panel_alertas_right, f"{id_page}_alertas_right"),
                ],
                hidden=True,
            ),
            my_div(
                style_div_alertas,
                f"{id_page}_panel_configuracion_alertas",
                [
                    my_div(
                        style_panel_parkings_configuration_table,
                        f"{id_page}_alertas_tabla",
                    ),
                    my_div(
                        style_div_button_config_accept,
                        "",
                        my_button(
                            f"{id_page}_alertas_accept",
                            "Guardar Cambios",
                            className="btn-accept",
                            n_clicks=0,
                        ),
                    ),
                ],
                hidden=True,
            ),
        ],
    ),
]
