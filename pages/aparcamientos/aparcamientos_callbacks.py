import random
from datetime import datetime, timedelta

import plotly.express as px
from dash import callback, dcc, html
from dash.dependencies import Input, Output, State
from pandas import DataFrame, read_csv

from assets.app_css import *
from assets.my_dash.my_html.my_div import my_div
from pages.aparcamientos.aparcamientos_utils import crear_df, mapa, tabla
from pages.listas import (columnas_monitorizacion_parking,
                          style_data_conditional_monitorizacion_parking)

id_page = "aparcamientos"


@callback(
    [
        Output(f"{id_page}_button_aparcamientos", "n_clicks"),
        Output(f"{id_page}_button_monitorizacion", "n_clicks"),
        Output(f"{id_page}_button_prevision", "n_clicks"),
        Output(f"{id_page}_button_aparcamientos", "style"),
        Output(f"{id_page}_button_monitorizacion", "style"),
        Output(f"{id_page}_button_prevision", "style"),
        Output(f"{id_page}_panel_aparcamientos", "hidden"),
        Output(f"{id_page}_panel_monitorizacion", "hidden"),
        Output(f"{id_page}_panel_prevision", "hidden"),
    ],
    [
        Input(f"{id_page}_button_aparcamientos", "n_clicks"),
        Input(f"{id_page}_button_monitorizacion", "n_clicks"),
        Input(f"{id_page}_button_prevision", "n_clicks"),
    ],
    [
        State(f"{id_page}_button_aparcamientos", "style"),
        State(f"{id_page}_button_monitorizacion", "style"),
        State(f"{id_page}_button_prevision", "style"),
    ],
)
def display_page(
    click_aparcamientos,
    click_monitorizacion,
    click_prevision,
    style_aparcamientos,
    style_monitorizacion,
    style_prevision,
):
    style_aparcamientos["border-bottom"] = "2px solid #919394"
    style_monitorizacion["border-bottom"] = "2px solid #919394"
    style_prevision["border-bottom"] = "1px solid black"
    panel_aparcamientos_hidden = True
    panel_monitorizacion_hidden = True
    panel_prevision_hidden = False
    if click_aparcamientos:
        style_aparcamientos["border-bottom"] = "1px solid black"
        style_monitorizacion["border-bottom"] = "2px solid #919394"
        style_prevision["border-bottom"] = "2px solid #919394"
        panel_aparcamientos_hidden = False
        panel_monitorizacion_hidden = True
        panel_prevision_hidden = True
    if click_monitorizacion:
        style_aparcamientos["border-bottom"] = "2px solid #919394"
        style_monitorizacion["border-bottom"] = "1px solid black"
        style_prevision["border-bottom"] = "2px solid #919394"
        panel_aparcamientos_hidden = True
        panel_monitorizacion_hidden = False
        panel_prevision_hidden = True
    return [
        0,
        0,
        0,
        style_aparcamientos,
        style_monitorizacion,
        style_prevision,
        panel_aparcamientos_hidden,
        panel_monitorizacion_hidden,
        panel_prevision_hidden,
    ]


@callback(
    Output(f"{id_page}_parking_tabla", "children"),
    Input(f"{id_page}_button_aparcamientos", "n_clicks"),
)
def sel_ho(pathname):
    df = crear_df()
    return tabla(
        f"{id_page}_tab_parking",
        df,
        columnas_monitorizacion_parking,
        style_table,
        style_header,
        style_data,
        style_data_conditional_monitorizacion_parking,
        15,
    )


@callback(
    [
        Output(f"{id_page}_parking_mapa", "children"),
        Output(f"{id_page}_tab_parking", "selected_rows"),
    ],
    Input(f"{id_page}_tab_parking", "selected_rows"),
    prevent_initial_call=True,
)
def sel_ho(selected_rows):
    df = read_csv("parking_information.csv")[["description", "lat", "long"]]
    zoom = 10
    if len(selected_rows) > 0:
        df = df.iloc[selected_rows]
        if len(selected_rows) == 1:
            zoom = 14
    return [
        dcc.Graph(
            figure=mapa(df, "markers", 10, "description", zoom),
            style=style_graph,
        ),
        selected_rows,
    ]


######### monitorizacion por aparcamiento
# Mapa
@callback(
    Output("mapa_monitorizacion_por_loc", "children"),
    Input("selector_localizacion_parking", "value"),
)
def sel_ho(b_selector):
    map = ""
    if b_selector:
        df = read_csv("parking_information.csv")[["description", "lat", "long"]]
        df = df[df["description"] == b_selector]
        map = dcc.Graph(
            figure=mapa(df, "markers", 13, "description", 14),
            style={
                "margin-left": "1%",
                "margin-top": "1%",
                "height": "97.5%",
                "width": "98%",
            },
        )
    return map


# Tarta
@callback(
    Output("tarta_monitorizacion_por_loc", "children"),
    Input("selector_localizacion_parking", "value"),
)
def sel_ho(sel):
    tarta = ""
    df = crear_df()
    df = df[df["description"] == sel]
    ocupacion = df["porcentaje"].iloc[0]

    df_tarta = DataFrame(
        {"Estado": ["Libre", "Ocupado"], "Ocupacion": [100 - ocupacion, ocupacion]}
    )
    tarta = dcc.Graph(
        figure=px.pie(
            df_tarta,
            values="Ocupacion",
            names=["Libre", "Ocupado"],
            title="",
            hover_data=None,
            color="Estado",
            hole=0.05,
            color_discrete_sequence=["#6EC15F", "#CD774F"],
        )
        .update_layout(
            height=280,
            margin=dict(t=30, l=0, b=0, r=100),
        )
        .update_traces(
            hovertemplate="<br>%{customdata[0]}",
        ),
        style={
            "width": "80%",
            "height": "95%",
        },
    )
    return tarta


@callback(
    [
        Output("estado", "children"),
        Output("barrera", "children"),
        Output("acceso", "children"),
        Output("miembros", "children"),
        Output("empleados", "children"),
        Output("minusvalidos", "children"),
        Output("ubicacion", "children"),
        Output("direccion", "children"),
        Output("actualizado", "children"),
    ],
    Input("selector_localizacion_parking", "value"),
)
def sel_ho(state_selector):
    footer = ["", "", ""]
    kpis = ["", "", "", "", "", ""]
    style_divs = [
        style_div_kpi_status,
        style_div_kpi_barrera,
        style_div_kpi_acceso,
        style_div_kpi_miembros,
        style_div_kpi_empleados,
        style_div_kpi_minusvalidos,
    ]

    df_inf = read_csv("parking_information.csv")
    df_inf = df_inf[df_inf["description"] == state_selector]
    valores = list(
        df_inf.iloc[0][
            [
                "status",
                "barrier_access",
                "free_access",
                "for_members",
                "for_employees",
                "for_disabled",
            ]
        ]
    )

    # kpis
    for i, valor in enumerate(valores):
        kpi = valor
        if kpi in ("Abierto", "Si"):
            style_divs[i]["background"] = "linear-gradient(50deg, #2FAC4E, #96CD8B)"
        else:
            style_divs[i]["background"] = "linear-gradient(50deg, #EA3E17, #CD9D8B)"
        kpis[i] = my_div(style_divs[i], "", f"{kpi}")

    # footer
    hora = datetime.now() - timedelta(minutes=random.randint(5, 180))
    ulr = f"https://www.google.com/maps/search/?api=1&query={state_selector + ',Chiclana de la Frontera'}"
    footer = [
        state_selector,
        html.A(
            df_inf["street_address"].iloc[0],
            href=ulr,
            target="blank",
            style={"color": "white"},
        ),
        str(hora).split(".")[0].replace("T", " "),
    ]
    return kpis + footer
