from assets.app_css import *
from assets.my_dash.my_dbc.my_button import my_button
from assets.my_dash.my_dcc.my_dropdown import my_dropdown
from assets.my_dash.my_html.my_div import my_div
from pages.aparcamientos.aparcamientos_callbacks import *
from pages.aparcamientos.prevision_callbacks import *
from pages.listas import lista_de_localizaciones

id_page = "aparcamientos"

layout = [
    my_div(
        style_panel_superior,
        "",
        [
            my_div(
                style,
                "",
                my_button(
                    f"{id_page}_button_{id_parking}",
                    name,
                    className=" btn-panel my-button",
                    n_clicks=clicks,
                ),
            )
            for name, id_parking, style, clicks in zip(
                ["Monitorización Global", "Monitorización por Aparcamiento", "Previsión"],
                ["aparcamientos", "monitorizacion", "prevision"],
                [
                    style_div_button_aparcamientos,
                    style_div_button_monitorizacion,
                    style_div_button_monitorizacion,
                ],
                [1, 0, 0],
            )
        ],
    ),
    my_div(
        style_panel_inferior,
        "",
        [
            my_div(
                style_panel_resultados,
                f"{id_page}_panel_aparcamientos",
                [
                    my_div(style_panel_tabla, f"{id_page}_parking_tabla"),
                    my_div(style_panel_mapa, f"{id_page}_parking_mapa"),
                ],
            ),
            my_div(
                style_panel_resultados,
                f"{id_page}_panel_monitorizacion",
                [
                    my_div(
                        style_div_drop_down,
                        "",
                        my_dropdown(
                            "selector_localizacion_parking",
                            style_drop_down,
                            options=lista_de_localizaciones,
                            value=lista_de_localizaciones[0],
                        ),
                    ),
                    my_div(
                        style_panel_titulos,
                        "",
                        [
                            my_div(
                                style_panel_plazas,
                                "",
                                "Plazas",
                            ),
                            my_div(
                                style_div_titulos,
                                "",
                                [
                                    my_div(style_div_kpi, "", "Estado"),
                                    my_div(style_div_kpi, "", "Barrera"),
                                    my_div(style_div_kpi, "", "Acceso Libre"),
                                    my_div(
                                        style_div_kpi_der_margen, "", "Para Miembros"
                                    ),
                                    my_div(style_div_kpi_der, "", "Para Empleados"),
                                    my_div(style_div_kpi_der, "", "Para Minusválidos"),
                                ],
                            ),
                            my_div(
                                style_div_titulos,
                                "",
                                [
                                    my_div(style_div_res_kpi, "estado"),
                                    my_div(style_div_res_kpi, "barrera"),
                                    my_div(style_div_res_kpi, "acceso"),
                                    my_div(style_div_res_kpi_margen, "miembros"),
                                    my_div(style_div_res_kpi, "empleados"),
                                    my_div(style_div_res_kpi, "minusvalidos"),
                                ],
                            ),
                        ],
                    ),
                    my_div(
                        style_panel_graficos,
                        "",
                        [
                            my_div(
                                style_panel_mapa_monitorizacion,
                                "mapa_monitorizacion_por_loc",
                            ),
                            my_div(
                                style_panel_tarta,
                                "",
                                [
                                    my_div(style_titulo_tarta, "", "Ocupación"),
                                    my_div(
                                        style_tarta,
                                        "tarta_monitorizacion_por_loc",
                                    ),
                                ],
                            ),
                        ],
                    ),
                    my_div(
                        style_panel_footer,
                        "",
                        [
                            my_div(
                                style_div_titulos,
                                "",
                                [
                                    my_div(style_footer, "", "Ubicación"),
                                    my_div(style_footer, "", "Dirección"),
                                    my_div(style_footer, "", "Actualizado"),
                                ],
                            ),
                            my_div(
                                style_div_titulos,
                                "",
                                [
                                    my_div(style_div, "ubicacion"),
                                    my_div(style_div, "direccion"),
                                    my_div(style_div, "actualizado"),
                                ],
                            ),
                        ],
                    ),
                ],
                hidden=True,
            ),
            my_div(
                style_panel_resultados,
                f"{id_page}_panel_prevision",
                [
                    my_div(
                        style_div_drop_down,
                        "",
                        my_dropdown(
                            "selector_drop_prevision",
                            style_drop_down,
                            options=lista_de_localizaciones,
                            value=lista_de_localizaciones[0],
                        ),
                    ),
                    my_div(style_panel_prevision, "prevision_up"),
                    my_div(style_panel_prevision, "prevision_down"),
                ],
                hidden=True,
            ),
        ],
    ),
]
