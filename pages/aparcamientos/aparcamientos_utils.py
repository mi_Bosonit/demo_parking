import random
from datetime import datetime, timedelta

import dash_bootstrap_components as dbc
import plotly.graph_objects as go
from dash import dash_table
from pandas import read_csv, read_json


def crear_df(data):
    df = read_json(data["parking"])
    df["actualizado"] = [
        datetime.now() - timedelta(minutes=random.randint(5, 360)) for x in range(15)
    ]
    df["Hora"] = df["actualizado"].apply(lambda x: x.time())
    df["Fecha"] = df["actualizado"].apply(lambda x: x.date())
    df["actualizado"] = df["actualizado"].apply(lambda x: str(x).split(".")[0])

    df = df.assign(
        porcentaje_ocupacion=lambda x: (
            (x.total_spot_number - x.available_spot_number) / (x.total_spot_number)
        )
    )
    df["porcentaje"] = df["porcentaje_ocupacion"].apply(lambda x: int(x * 100))
    df["status"] = df["status"].apply(lambda x: "Abierto" if x == "open" else "Cerrado")
    df["actualizado"] = df["actualizado"].apply(lambda x: " ".join(str(x).split("T")))
    return df


def crear_df():
    df_inf = read_csv("parking_information.csv")
    df_moni = read_csv("monitorizacion.csv")
    df = df_inf.merge(df_moni, how="left", on="id")
    df["actualizado"] = [
        datetime.now() - timedelta(minutes=random.randint(5, 360)) for x in range(15)
    ]
    df["Hora"] = df["actualizado"].apply(lambda x: x.time())
    df["Fecha"] = df["actualizado"].apply(lambda x: x.date())
    df["actualizado"] = df["actualizado"].apply(lambda x: str(x).split(".")[0])

    df = df.assign(
        porcentaje_ocupacion=lambda x: (
            (x.total_spot_number - x.available_spot_number) / (x.total_spot_number)
        )
    )
    df["porcentaje"] = df["porcentaje_ocupacion"].apply(lambda x: int(x * 100))
    df["status"] = df["status"].apply(
        lambda x: "Abierto" if x == "Abierto" else "Cerrado"
    )
    df["actualizado"] = df["actualizado"].apply(lambda x: " ".join(str(x).split("T")))
    return df


def mapa(df, mode, size, text, zoom=13, lat=0, lon=0):
    if lat == 0:
        lat = df["lat"].iloc[0]
    if lon == 0:
        lon = df["long"].iloc[0]
    fig = go.Figure(
        go.Scattermapbox(
            lat=df["lat"],
            lon=df["long"],
            mode=mode,
            marker=go.scattermapbox.Marker(size=size, color="green"),
            text=df[text],
        )
    )
    fig.update_layout(
        hovermode="closest",
        mapbox_style="open-street-map",
        mapbox={"zoom": zoom, "center": {"lon": lon, "lat": lat}},
        autosize=True,
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
    )
    return fig


def tabla(
    id,
    df,
    columnas_tabla,
    style_table,
    style_header,
    style_data,
    style_data_conditional,
    page_size=14,
    row_selectable="multi",
    editable=False,
):
    return dbc.Container(
        [
            dash_table.DataTable(
                df.to_dict("records"),
                columns=columnas_tabla,
                id=id,
                row_selectable=row_selectable,
                page_size=page_size,
                selected_rows=[],
                style_table=style_table,
                style_data=style_data,
                style_header=style_header,
                style_cell={"textAlign": "center"},
                style_data_conditional=style_data_conditional,
                editable=editable,
            )
        ]
    )


def tabla_configuracion(
    id,
    df,
    columnas_tabla,
    style_table,
    style_header,
    style_data,
    style_data_conditional,
    dropdown_options,
    page_size=14,
    row_selectable="multi",
    editable=False,
):   
    return dbc.Container(
        [
            dash_table.DataTable(
                df.to_dict("records"),
                columns=columnas_tabla,
                id=id,
                row_selectable=row_selectable,
                page_size=page_size,
                selected_rows=[],
                row_deletable=True,
                dropdown=dropdown_options,
                style_table=style_table,
                style_data=style_data,
                style_header=style_header,
                style_cell={"textAlign": "center"},
                style_data_conditional=style_data_conditional,
                editable=editable,             
            )
        ]
    )
