import random
from datetime import datetime, timedelta

import pandas as pd
import plotly.express as px
from dash import dcc

from assets.app_css import font_family


def crear_df_prevision():
    now = datetime.now()
    horas = [now + timedelta(minutes=x) for x in [15, 30, 45, 60, 75, 90, 105, 120]]
    horas = [str(x).split(" ")[1].split(".")[0] for x in horas]
    df_prevision = pd.DataFrame({"horas": horas})
    hora = int(horas[0].split(":")[0])
    match hora:
        case 23 | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7:
            df_prevision["plazas"] = [165 for x in range(8)]
        case 8 | 9:
            df_prevision["plazas"] = [random.randint(100, 163) for x in range(8)]
        case 10 | 11 | 12 | 13:
            df_prevision["plazas"] = [random.randint(29, 50) for x in range(8)]
        case 14 | 15 | 16:
            df_prevision["plazas"] = [random.randint(80, 100) for x in range(8)]
        case 17 | 18 | 19:
            df_prevision["plazas"] = [random.randint(29, 50) for x in range(8)]
        case 20 | 21:
            df_prevision["plazas"] = [random.randint(50, 90) for x in range(8)]
        case _:
            df_prevision["plazas"] = [random.randint(95, 150) for x in range(8)]
    return df_prevision


def crear_df_fiabilidad():
    start = datetime.now()
    horas = [start + timedelta(minutes=x) for x in range(0, 1440, 30)]
    horas = [str(x).split(" ")[1].split(".")[0] for x in horas]
    horas = [":".join(hora.split(":")[:2]) for hora in horas]

    prevision = []
    real = []
    for hora in horas:
        hora = int(hora.split(":")[0])
        match hora:
            case 23 | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7:
                n_real = 165
                aleatorio = 0
            case 8 | 9:
                n_real = random.randint(100, 163)
                aleatorio = int(random.uniform(-1, 3))
            case 10 | 11 | 12 | 13:
                n_real = random.randint(25, 50)
                aleatorio = int(random.uniform(-5, 5))
            case 14 | 15 | 16:
                n_real = random.randint(80, 100)
                aleatorio = int(random.uniform(-2, 2))
            case 17 | 18 | 19:
                n_real = random.randint(25, 50)
                aleatorio = int(random.uniform(-5, 5))
            case 20 | 21:
                n_real = random.randint(50, 90)
                aleatorio = int(random.uniform(-2, 2))
            case _:
                n_real = random.randint(90, 150)
                aleatorio = int(random.uniform(-1, 1))
        real.append(n_real)
        prevision.append(n_real + aleatorio)
    return pd.DataFrame({"real": real, "prevision": prevision, "hora": horas})


def panel_prevision(value):
    df_prevision = crear_df_prevision()
    max_y = df_prevision["plazas"].max()

    fig = (
        px.bar(
            df_prevision,
            x="horas",
            y="plazas",
            text="plazas",
            color_discrete_sequence=px.colors.qualitative.Pastel,
        )
        .update_xaxes(title_text="<b>Hora<b>", showgrid=False)
        .update_yaxes(title_text="<b>Plazas Libres<b>", showgrid=False)
        .update_yaxes(zeroline=False)
        .update_traces(textposition="outside")
        .update_layout(
            title={
                "text": f"<b>Previsión {value} (próximas 2 horas)<b>",
                "y": 1,
                "yanchor": "top",
                "font": {"size": 16},
            },
            title_pad=dict(t=5),
            plot_bgcolor="white",
            paper_bgcolor="white",
            yaxis_range=[0, max_y + max_y * 0.1],
            margin={"r": 10, "t": 25, "l": 100, "b": 10},
        )
    )

    return dcc.Graph(
        figure=fig,
        style={
            "position": "relative",
            "left": "1%",
            "top": "2%",
            "height": "96%",
            "width": "98%",
            "font-family": font_family,
        },
    )


def panel_fiabilidad():
    df = crear_df_fiabilidad()
    max_y = df["real"].max()

    fig = (
        px.line(
            df,
            x="hora",
            y=["prevision", "real"],
            color_discrete_sequence=px.colors.qualitative.Pastel,
        )
        .update_xaxes(title_text="<b>Hora<b>", showgrid=False)
        .update_yaxes(title_text="<b>Plazas Libres<b>", showgrid=False)
        .update_yaxes(zeroline=False)
        .update_layout(
            title={
                "text": "<b>Fiabilidad del modelo en las últimas 24 horas<b>",
                "y": 1,
                "yanchor": "top",
                "font": {"size": 16},
            },
            title_pad=dict(t=5),
            plot_bgcolor="white",
            paper_bgcolor="white",
            yaxis_range=[0, max_y + max_y * 0.1],
            margin={"r": 10, "t": 25, "l": 100, "b": 10},
        )
    )
    return dcc.Graph(
        figure=fig,
        style={
            "position": "relative",
            "left": "1%",
            "top": "2%",
            "height": "96%",
            "width": "98%",
            "font-family": font_family,
        },
    )
