from dash import callback
from dash.dependencies import Input, Output

from pages.aparcamientos.prevision_utils import (panel_fiabilidad,
                                                 panel_prevision)


@callback(
    [
        Output("prevision_up", "children"),
        Output("prevision_down", "children"),
    ],
    [
        Input("selector_drop_prevision", "value"),
    ],
)
def display_page(value):
    return [panel_prevision(value), panel_fiabilidad()]
