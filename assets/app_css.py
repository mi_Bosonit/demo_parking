import screeninfo
import dash_dangerously_set_inner_html

screen_info = screeninfo.get_monitors()[0]
screen_width = screen_info.width
screen_height = screen_info.height
font_size_in_pixels = min(screen_width, screen_height) * 0.015

background_content = "linear-gradient(90deg,#d8dfe4 0%, #d1e1ec 80%)"
background_content2 = "linear-gradient(90deg,#d1e1ec 0%,#d8dfe4  99%)"

background_dropdown = (
    "radial-gradient(circle farthest-side at bottom left, #b0d8d3 0%, #051f28 95%"
)
background_in_dropdown = (
    "radial-gradient(circle farthest-side at bottom left, #bfeef6 0%, #5c8d95 95%)"
)
background_up_panel = "rgba(255,255,255,.5)"  # "linear-gradient(90deg, #04212c 0%, #062741 70%, #0f5779 90%)"
backgroud_left_panel = (
    "#f1f1f1"  # "linear-gradient(90deg,#165b92 0%, #2c81a9 10%, #04212c 99.9%)"
)

font_family = "Roboto, Helvetica, Arial, sans-serif"
color_boton_1 = "#acf4ed"
color_code = "#21F5F8"
color_axis_scatter_3d = "#0A3A4B"
color_bgcolor_scatter_3d = "#C5E6F2"
color_celdas_header = "linear-gradient(0deg,#d9fefe 0%, #e4e9e9 80%)"
color_celdas_tabla = "linear-gradient(90deg, #d3dcdc 0%, #f8f8f8 40%, #d3dcdc 80%)"
##### app.py #####
style_app = {}

style_content_app = {
    "position": "absolute",
    "top": "0%",
    "left": "0%",
    "width": "100%",
    "height": "100%",
    "background": "transparent",  # "#f1f1f1"
}

##### aparcamientos.py #####
# panel superior
style_panel_superior = {
    "position": "relative",
    "width": "100%",
    "height": "7%",
    "background": "transparent",
}

style_div_button_aparcamientos = {
    "position": "relative",
    "float": "left",
    "margin-top": "1%",
    "margin-left": "3%",
    "width": "30.9%",
    "height": "65%",
    "background": "#5B676B",
    "font-family": font_family
}



style_div_button_monitorizacion = style_div_button_aparcamientos.copy()
style_div_button_monitorizacion["margin-left"] = "0.21%"

# panel inferior
style_panel_inferior = {
    "position": "relative",
    "margin-top": "1%",
    "width": "100%",
    "height": "91%",
}

style_panel_resultados = {
    "position": "relative",
    "weight": "100%",
    "height": "100%",
    "background": "#f1f1f1",
}

style_panel_tabla = {
    "position": "relative",
    "float": "left",
    "left": "1%",
    "top": "1%",
    "height": "98%",
    "width": "75%",
    "border-radius": "15px",
    "background": "rgba(255,255,255,255)",
}

style_panel_mapa = {
    "position": "relative",
    "float": "left",
    "left": "2%",
    "top": "1%",
    "height": "98%",
    "width": "22%",
    "border-radius": "10px",
    "background": "rgba(255,255,255,255)",
}

style_graph = {
    "margin-left": "2%",
    "margin-top": "2%",
    "height": "98%",
    "width": "96%",
}

#### Monitorizacion por localizacion
style_div_drop_down = {
    "position": "relative",
    "left": "2%",
    "width": "95.6%",
    "height": "6.6%",
    "padding": "2px 2px 0px 1px",
    "background": "#8B9598",
}

style_drop_down = {
    "background": "#f1f1f1",
    "font-size": "1.1vmax",
    "font-family": font_family,
}

style_panel_titulos = {
    "margin-left": "2%",
    "margin-top": "1%",
    "width": "97.2%",
    "height": "15%",
    "font-family": font_family,
}

style_panel_plazas = {
    "position": "relative",
    "margin-left": "72%",
    "width": "27%",
    "height": "25%",
    "font-size": "1.3vmax",
    "font-family": font_family,
}

style_div_titulos = {
    "width": "100%",
    "height": "37%",
    "font-family": font_family,
    "background": "transparent",
}

style_div_res_footer = {}

style_div_kpi = {
    "float": "left",
    "width": "15%",
    "font-size": "1.2vmax",
    "text-align": "center",
    "font-family": font_family,
}

style_div_kpi_der_margen = {
    "float": "left",
    "margin-left": "10%",
    "margin-top": "0.3%",
    "width": "15%",
    "font-size": "1.1vmax",
    "font-family": font_family,
}

style_div_kpi_der = {
    "float": "left",
    "margin-top": "0.3%",
    "width": "15%",
    "font-size": "1.1vmax",
    "font-family": font_family,
}

style_div_res_kpi = {"float": "left", "width": "15%", "font-family": font_family}

style_div_res_kpi_margen = {
    "float": "left",
    "margin-left": "7%",
    "width": "15%",
}

style_panel_graficos = {
    "position": "relative",
    "margin-top": "0.1%",
    "margin-left": "2%",
    "width": "97.5%",
    "height": "62%",
}

style_panel_mapa_monitorizacion = {
    "float": "left",
    "width": "46%",
    "height": "99%",
    "border-radius": "10px",
    "background": "white",
}

style_panel_tarta = {
    "float": "left",
    "margin-left": "5%",
    "width": "45%",
    "height": "99%",
    "border-radius": "20px",
    "background": "white",
}

style_titulo_tarta = {
    "margin-top": "2%",
    "width": "90%",
    "height": "8%",
    "font-size": "1.5vmax",
    "text-align": "center",
    "font-family": font_family,
}

style_tarta = {
    "margin-top": "0.1%",
    "margin-left": "12%",
    "height": "88%",
    "width": "98%",
}

style_panel_footer = {
    "width": "93%",
    "margin-left": "2%",
    "margin-top": "1.5%",
    "height": "8%",
}

style_div = {
    "float": "left",
    "width": "33%",
    "font-size": "1.3vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
    "font-family": font_family,
    "background": "linear-gradient(90deg,#165b92 0%, #2c81a9 10%, #04212c 99.9%)",
}

style_footer = {
    "float": "left",
    "width": "33%",
    "font-size": "1.2vmax",
    "color": "#3858B2",
    "text-align": "center",
    "font-family": font_family,
}

# prevision    
style_panel_prevision = {
    "position": "relative",
    "top": "1%",
    "width": "100%",
    "height": "45.5%",
    "font-size": "2vmax",
}

# callbacks
style_div_kpi_status = {
    "float": "left",
    "width": "97%",
    "font-size": "1.3vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
    "font-family": font_family,
}

style_div_kpi_barrera = {
    "float": "left",
    "width": "97%",
    "font-size": "1.3vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
    "font-family": font_family,
}

style_div_kpi_acceso = {
    "float": "left",
    "width": "97%",
    "font-size": "1.3vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
    "font-family": font_family,
}

style_div_kpi_miembros = {
    "float": "left",
    "width": "97%",
    "font-size": "1.3vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
    "font-family": font_family,
}

style_div_kpi_empleados = {
    "float": "left",
    "width": "97%",
    "font-size": "1.3vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
    "font-family": font_family,
}

style_div_kpi_minusvalidos = {
    "float": "left",
    "width": "97%",
    "font-size": "1.3vmax",
    "color": "white",
    "border-radius": "7px 7px 5px 5px",
    "text-align": "center",
    "font-family": font_family,
}

#### admin.py ####
style_panel_up_admin = {
    "position": "relative",
    "width": "100%",
    "height": "6%",
}

style_div_button_panel_up = {
    "float": "left",
    "margin-top": "1%",
    "margin-left": "3%",
    "width": "30.9%",
    "height": "79.7%",
    "background": "#5B676B",
}

style_div_button_panel_up_right = style_div_button_panel_up.copy()
style_div_button_panel_up_right["margin-left"] = "0.21%"

style_name_buttom_panel_up = {
    "float": "left",
    "margin-top": "1%",
    "width": "93%",
}

style_icon_buttom_panel_up = {
    "float": "left",
    "width": "7%",
    "height": "90%"
}

style_panel_buttons_parking = {
    "margin-left": "5.2%",
    "margin-top": "0.7%",
    "width": "25%",
    "height": "18.5%",
}

style_panel_buttons_datos = style_panel_buttons_parking.copy()
style_panel_buttons_datos["margin-left"] = "37%"

style_panel_buttons_cuentas = style_panel_buttons_parking.copy()
style_panel_buttons_cuentas["margin-left"] = "69%"

style_div_button_parking = {    
    "margin-top": "0.5%",
    "margin-left": "2.2%",
    "width": "95%",
    "height": "23%",
    "font-family": font_family,
}

style_panel_down_admin = {
    "position": "relative",
    "top": "1%",
    "width": "100%",
    "height": "92%",
}

style_panel_parkings = {
    "position": "relative",
    "width": "100%",
    "height": "100%",
}

style_panel_parkings_configuration = {
    "position": "relative",
    "width": "100%",
    "height": "100%",
}

style_panel_parkings_configuration_table = {
    "position": "relative",
    "left": "3%",
    "width": "94%",
    "height": "90%",
    "background": "white",
    "border-radius": "20px 20px 0px 0px",
    "overflow-y": "auto",
}

style_div_button_config_accept = {
    "margin-left": "3%",
    "width": "94%",
    "height": "10%",
    "background": "white",
    "border-radius": "0px 0px 20px 20px",    
    "font-family": font_family,
}

style_panel_parkings_up = {
    "position": "relative",
    "width": "99.5%",
    "height": "40.8%",
}

style_panel_parkings_down = {
    "position": "relative",
    "width": "99.5%",
    "height": "58%",
}

style_div_figures_stats = {
    "position": "relative",
    "width": "100%",
    "height": "100%",
}

style_figures_estats = {
    "float": "left",
    "margin-left": "5%",
    "margin-top": "2%",
    "width": "26%",
    "height": "80%",
}

style_div_alertas = {
    "position": "relative",
    "width": "99.6%",
    "height": "100%",
}

style_panel_alertas_left = {
    "float": "left",
    "margin-left": "2%",
    "width": "50%",
    "height": "99.5%",
    "border-radius": "10px",
    "background": "white",
    "overflow-y": "auto",
}

style_panel_alertas = {
    "position": "relative",
    "top": "8%",
    "left": "25%",
    "width": "70%",
    "height": "90%",
}

style_alerta = {
    "margin-top": "1%",
    "margin-left": "2%",
    "width": "100%",
    "font-family": font_family,
}

style_name_parking = {"float": "left", "width": "100%", "font-family": font_family}

style_plazas_parking = {"width": "100%", "font-family": font_family}

style_registro_parking = {"width": "100%", "font-family": font_family}

style_panel_alertas_right = {
    "float": "left",
    "margin-left": "1%",
    "width": "46%",
    "height": "100%",
    "border-radius": "8px 8px 7px 7px",
    "background": "white",
}

style_graph_admin = {
    "position": "relative",
    "left": "1%",
    "top": "1%",
    "height": "98%",
    "width": "98%",
}
#### dash_table
style_table = {
    "border": "1.5px solid rgba(36, 41, 46, 0.12)",
    "margin-left": "1%",
    "margin-top": "1%",
    "width": "98%",
    "height": "98%",
}

style_table_configuracion_parkings = {
    "border": "1.5px solid rgba(36, 41, 46, 0.12)",
    "margin-left": "4%",
    "margin-top": "2%",
    "width": "92%",
    "height": "90%",
}

style_data = {
    "padding": "6px",
    "align-items": "center",
    "width": "8vmax",
    "min-width": "8vmax",
    "font-family": font_family,
    "font-size": "0.95vmax",
    "border-right": "1.5px solid rgba(36, 41, 46, 0.12)",
    "background": color_celdas_tabla,
}
style_header = {
    "padding": "6px",
    "color": "#143CB8",
    "font-family": font_family,
    "font-size": "1.1vmax",
    "border-bottom": "1.5px solid rgba(36, 41, 46, 0.12)",
    "border-right": "1.5px solid rgba(36, 41, 46, 0.12)",
    "background": color_celdas_header,
}
